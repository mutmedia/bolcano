Shader "Raymarching/Forward_ModWorld"
{

Properties
{
    [Header(PBS)]
    _Color("Color", Color) = (1.0, 1.0, 1.0, 1.0)
    _Metallic("Metallic", Range(0.0, 1.0)) = 0.5
    _Glossiness("Smoothness", Range(0.0, 1.0)) = 0.5

    [Header(Pass)]
    [Enum(UnityEngine.Rendering.CullMode)] _Cull("Culling", Int) = 2

    [Toggle][KeyEnum(Off, On)] _ZWrite("ZWrite", Float) = 1

    [Header(Raymarching)]
    _Loop("Loop", Range(1, 100)) = 30
    _MinDistance("Minimum Distance", Range(0.001, 0.1)) = 0.01
    _DistanceMultiplier("Distance Multiplier", Range(0.001, 2.0)) = 1.0
    _ShadowLoop("Shadow Loop", Range(1, 100)) = 30
    _ShadowMinDistance("Shadow Minimum Distance", Range(0.001, 0.1)) = 0.01
    _ShadowExtraBias("Shadow Extra Bias", Range(0.0, 0.1)) = 0.0

// @block Properties
[Header(Additional Parameters)]
_Grid("Grid", 2D) = "" {}
// @endblock
}

SubShader
{

Tags
{
    "RenderType" = "Opaque"
    "Queue" = "Geometry"
    "DisableBatching" = "True"
}

Cull [_Cull]

CGINCLUDE

#define WORLD_SPACE

#define OBJECT_SHAPE_NONE

#define CAMERA_INSIDE_OBJECT

#define USE_RAYMARCHING_DEPTH

#define SPHERICAL_HARMONICS_PER_PIXEL

#define DISTANCE_FUNCTION DistanceFunction
#define PostEffectOutput SurfaceOutputStandard
#define POST_EFFECT PostEffect

#include "Assets\uRaymarching\Shaders\Include/Common.cginc"

// @block DistanceFunction
inline float3 RandomOffset(float seed, float maxOffset) {
    float random1 = -1 + 2 * Rand(float2(seed, 1242134));
    float random2 = -1 + 2 * Rand(float2(seed, 421432));
    return float3(random1 * maxOffset, random2 * maxOffset, 0);

}

inline float RandomSquare(float seed, float3 pos, float2 size) {

    return Box(pos, float3(size.xy, 1));
}

inline float TeatroNacional(float3 pos)
{
  float2 repetition = float2(3.5, 3.5);
  float3 posRepeat = float3(Repeat(pos.xy, repetition), pos.z);
  float seed = Rand(floor(pos.xy / repetition));


  // Teatro nacional
  float tamanhoMaior = 1.;
  float tamanhoMedio = 2./3.;
  float tamanhoMenor = 1./3.;
  float offsetBase = 2 * tamanhoMenor;
  float h = 0.4;
  if(seed < 0.5)
  {
    // 1 cubo
    float size = lerp(tamanhoMedio, tamanhoMaior, step(0.25, seed));
    return RandomSquare(
      seed,
      posRepeat + RandomOffset(seed, offsetBase),
      float2(size, size)
      );
  } else { // 2 ou 3 cubos
    {
      float random = Rand(float2(seed, 100));
      float randomAngle = floor(random * 4) * PI/2.;
      posRepeat = Rotate(posRepeat, randomAngle, float3(0, 0, 1));
    }

    float cubinho = RandomSquare(
      seed,
      posRepeat + RandomOffset(seed, offsetBase) +
      float3(-2 * tamanhoMenor, -2 * tamanhoMenor, 0),
      float2(tamanhoMenor, tamanhoMenor));

    // 2 cubos
    float tamanhoEsticado = lerp(tamanhoMedio, tamanhoMaior, step(0.75, seed));
    float cuboEsticado = RandomSquare(
        Rand(float2(seed, 5)),
        posRepeat + RandomOffset(seed, offsetBase) +
        float3(2 * tamanhoMenor, - (tamanhoMaior - tamanhoEsticado), 0),
        float2(tamanhoMenor, tamanhoEsticado)
      );

    // Terceiro cubo
    if(seed < 0.75) {
      float cubinho2 = RandomSquare(
        seed,
        posRepeat + RandomOffset(seed, offsetBase) +
        float3(-2 * tamanhoMenor, 2 * tamanhoMenor, 0),
        float2(tamanhoMenor, tamanhoMenor));

        cubinho = min(cubinho, cubinho2);
    }

    cubinho = min(cubinho, cuboEsticado);
    return cubinho;
  }
}

inline float DistanceFunction(float3 pos) {
  pos = Rotate(pos, PI/15, float3(1, 0, 0));
  float plane = Plane(pos - float3(0, 0, 1), float3(0, 0, -1));
  float teatro = TeatroNacional(pos);
  //return teatro;
  return min(teatro, plane);
}
// @endblock

// @block PostEffect
sampler2D _Grid;
float4 _Grid_ST;

inline void PostEffect(RaymarchInfo ray, inout PostEffectOutput o)
{
    //o.Emission += tex2D(_Grid, ray.endPos.xy * _Grid_ST.xy + _Grid_ST.zw);
    float ao = 1.0 - 1.0 * ray.loop / ray.maxLoop;
    o.Occlusion *= ao;
    o.Emission *= ao;
}
// @endblock

ENDCG

Pass
{
    Tags { "LightMode" = "ForwardBase" }

    ZWrite [_ZWrite]

    CGPROGRAM
    #include "Assets\uRaymarching\Shaders\Include/ForwardBaseStandard.cginc"
    #pragma target 3.0
    #pragma vertex Vert
    #pragma fragment Frag
    #pragma multi_compile_instancing
    #pragma multi_compile_fog
    #pragma multi_compile_fwdbase
    ENDCG
}

Pass
{
    Tags { "LightMode" = "ForwardAdd" }
    ZWrite Off 
    Blend One One

    CGPROGRAM
    #include "Assets\uRaymarching\Shaders\Include/ForwardAddStandard.cginc"
    #pragma target 3.0
    #pragma vertex Vert
    #pragma fragment Frag
    #pragma multi_compile_instancing
    #pragma multi_compile_fog
    #pragma skip_variants INSTANCING_ON
    #pragma multi_compile_fwdadd_fullshadows
    ENDCG
}

Pass
{
    Tags { "LightMode" = "ShadowCaster" }

    CGPROGRAM
    #include "Assets\uRaymarching\Shaders\Include/ShadowCaster.cginc"
    #pragma target 3.0
    #pragma vertex Vert
    #pragma fragment Frag
    #pragma fragmentoption ARB_precision_hint_fastest
    #pragma multi_compile_shadowcaster
    ENDCG
}

}

Fallback Off

CustomEditor "uShaderTemplate.MaterialEditor"

}