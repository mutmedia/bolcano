﻿using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class EfeitoLapis : MonoBehaviour
{
  public Material _material;

  void Start()
  {
  }

  void OnRenderImage(RenderTexture src, RenderTexture dest)
  {
    {
      Graphics.Blit(src, dest, _material);
    }
  }
}
