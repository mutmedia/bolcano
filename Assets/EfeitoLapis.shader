﻿Shader "Hidden/EfeitoLapis"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _LapisTex ("Lapis", 2D) = "white" {}
        color0 ("Color0", float) = 0.1
        color1 ("Color1", float) = 0.2
        color2 ("Color2", float) = 0.3
        color3 ("Color3", float) = 0.4
        color4 ("Color4", float) = 0.5
        color5 ("Color5", float) = 0.6
        color6 ("Color6", float) = 0.7
        color7 ("Color7", float) = 0.8
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            sampler2D _LapisTex;
            float color0;
            float color1;
            float color2;
            float color3;
            float color4;
            float color5;
            float color6;
            float color7;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                float l = col.a * (col.r * 0.3 + col.g * 0.59 + col.b * 0.11);

                float selection = 
                  step(l, color1) +
                  step(l, color2) +
                  step(l, color3) +
                  step(l, color4) +
                  step(l, color5) +
                  step(l, color6) +
                  step(l, color7);
                
                float2 selectionBase = float2(
                  selection % 4,
                  selection / 2
                );

                float2 selectionUV = (((selectionBase + i.uv) * float2(0.25, 0.5)) + float2(0, 0.5));//* float2(1./4., 1./2.);

                fixed4 texCol = tex2D(_LapisTex, selectionUV);
                //return l;
                return texCol;
            }
            ENDCG
        }
    }
}
