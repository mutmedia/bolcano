﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ToggleEfeitoLapis : MonoBehaviour
{
  public EfeitoLapis reg;
  public EfeitoLapis inv;

  // Update is called once per frame
  void Update()
  {
    if (Input.GetKeyDown(KeyCode.Q))
    {
      reg.enabled = false;
      inv.enabled = false;
    }
    if (Input.GetKeyDown(KeyCode.W))
    {
      reg.enabled = false;
      inv.enabled = true;
    }
    if (Input.GetKeyDown(KeyCode.E))
    {
      reg.enabled = true;
      inv.enabled = false;
    }
    if (Input.GetKeyDown(KeyCode.R))
    {
      reg.enabled = true;
      inv.enabled = true;
    }

  }
}
